from django.shortcuts import render
from .forms import fillform
from .models import fill
from django.http import HttpResponse,HttpResponseRedirect
def index(request):
    form = fillform(request.POST)
    data = fill.objects.all().order_by('-date')
    if form.is_valid():
        form.save()
        return HttpResponseRedirect('/')
    else:
        form = fillform()
    return render(request,'index.html',{'form':form,'data':data})
