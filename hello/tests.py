from django.test import TestCase
from django.test import Client
from django.urls import resolve
from hello.views import index
from hello.models import fill
from hello.forms import fillform
class story6test(TestCase):
    def test_story6_url_exist(self):
        resp = Client().get('/')
        self.assertEqual(resp.status_code, 200)
    
    def test_story_6_using_story_6_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')


    def test_story6_index_function(self):
        find = resolve('/')
        self.assertEqual(find.func, index)
    
    def test_model_form(self):
        new_activity = fill.objects.create(status ='status')
        count_fill = fill.objects.all().count()
        self.assertEqual(count_fill,1)
    def test_story6_post_success_and_render_result(self):
        test='Anonymous'
        resp_post = Client().post('/',{'status':test})
        self.assertEqual(resp_post.status_code, 302)
        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)
    def test_story6_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/', {'status': ''})
        self.assertEqual(response_post.status_code, 200)
        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

